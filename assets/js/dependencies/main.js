var Bulletin = {
    App : {},
};


$(document).ready(function() {
    //$('#api-table').dataTable();
    var date = new Date()
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
    
    var endDate = new Date('01/06/2018 10:00 AM'),
        ed = endDate.getDate(),
        em = endDate.getMonth(),
        ey = endDate.getFullYear();
        
    $("#preview-calendar").fullCalendar();
    var preEventList = [];
    var yy = getJSONCallback('/admin/get-list-events', function(data) {
        for(var i in data){
            var eventTitle = data[i].title;
            var eventStart = data[i].start_date;
            var eventEnd = data[i].end_date;
            var eventColor = data[i].colorTag;
           
            preEventList.push({
                'title': eventTitle,
                'start': eventStart,
                'end': eventEnd,
                'borderColor' : eventColor,
                'backgroundColor' : eventColor
            });
            
        }
      console.log("json",JSON.stringify(preEventList));
        $("#preview-calendar").fullCalendar( 'addEventSource', preEventList);

     });

    $('#eventTime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        }
    });

    $('#editEventTime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        },
        autoUpdateInput: true
    });

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    });
    $('#calendar').fullCalendar({
        height:'80%',
        timeFormat: 'h:mma'
    });
    var eventList = [];
    var gToken = window.localStorage.getItem("token");
    var x = getJSONCallback('/admin/get-list-events', function(data) {
        for(var i in data){
            var eventTitle = data[i].title;
            var eventStart = data[i].start_date;
            var eventEnd = data[i].end_date;
            var eventColor = data[i].colorTag;
           
            eventList.push({
                'title': eventTitle,
                'start': eventStart,
                'end': eventEnd,
                'borderColor' : eventColor,
                'backgroundColor' : eventColor
            });
            
        }
      
        $("#calendar").fullCalendar( 'addEventSource', eventList);

     });
     
    
    function getJSONCallback(apiURL, callback) {
        var self = this;
        var url  = apiURL;
        $.getJSON(url, function (list) {
            callback(list);
        });
    }
    function clock() {// We create a new Date object and assign it to a variable called "time".
        var time = new Date(),

            // Access the "getHours" method on the Date object with the dot accessor.
            hours = time.getHours(),

            // Access the "getMinutes" method with the dot accessor.
            minutes = time.getMinutes(),


            seconds = time.getSeconds(),

            status = 'AM';

            if(hours > 12){
                hours = hours - 12;
                status = 'PM';
            }
                

        document.querySelectorAll('.clock')[0].innerHTML = harold(hours) + ":" + harold(minutes) + ":" + harold(seconds) +" "+status;

        function harold(standIn) {
            if (standIn < 10) {
                standIn = '0' + standIn
            }
            return standIn;
        }
    }
    setInterval(clock, 1000);

    $('#frmAnnouncement').on('submit', function(e){
        e.preventDefault();
        window.localStorage.setItem("announcement_text", $('#announcement_text').val());
        window.localStorage.setItem("vision_text", $('#vision_text').val());
        window.localStorage.setItem("mission_text",$('#mission_text').val());
        
        $('#announcement_txt').html(localStorage.getItem('announcement_text'));
        $('#vision_txt').html(localStorage.getItem('vision_text'));
        $('#mission_txt').html(localStorage.getItem('mission_text'));
        
    });
    //window.localStorage.setItem("announcement_text", $('#announcement_text').val());
    $('#announcement_txt').html(localStorage.getItem('announcement_text'));
    $('#vision_txt').html(localStorage.getItem('vision_text'));
    $('#mission_txt').html(localStorage.getItem('mission_text'));

    
});
/**
 * 
 * @author Bryan Judelle Ramos
 * @description
 * 
 */
var oTable = $('#api-table');
var gToken = window.localStorage.getItem("token");
var gRole  = window.localStorage.getItem("role");

Bulletin.App.Admin = {
    
    fnDashboardLoader: function() {
        var self = this;

            self.addEventModalHandler();
            self.loadEventTableData();

            $('a.admin-nav').prop('href', '/admin?token=' + gToken);
            $('a.pending-nav').prop('href', '/admin/pending-events?token=' + gToken);
            $('a.preview-nav').prop('href', '/admin/event-preview?token=' + gToken);
            $('a.system-nav').prop('href', '/admin/system-management?token=' + gToken);

            if (gRole !== undefined && gRole === 'ADMIN')
                $('.adminApproval').removeClass('hide');
            else 
                $('.adminApproval').addClass('hide');

            $('#logout').on('click', function(e) {
                window.localStorage.clear();
                window.location.replace('/login');
            });
        },

    /**
     * 
     * 
     * 
     */
    editEventModalhandler: function (context, e) {
        var self       = this,
            tag        = $('<div />'),
            colorTag   = $('#curColorTag'),
            modalEdit  = $('#modal-edit'),
            editStatus = $('#editEventStatus'),
            editURL    = '/admin/get-event/',
            paramId    = $(context).data('value');

            modalEdit.modal({show: true});

            self.getJSONCallback(editURL + paramId, false, function(data) {
               
                $('#editEventTitle').val(data.title);
                $('#editEventTime').data('daterangepicker').setStartDate(new Date(data.start_date));
                $('#editEventTime').data('daterangepicker').setEndDate(new Date(data.end_date));
                $('#editEventDescription').val(data.description);
                $('#editEventColorTag').val(data.colorTag);
                
                (data.status === 'APPROVED') ? editStatus.prop('checked', true) : editStatus.prop('checked', false);
                    editStatus.iCheck('update');
                    tag.css({
                        'background-color': data.colorTag,
                        'border-color'    : data.colorTag,
                        'color'           : '#fff'
                    }).addClass('external-event');
                    tag.html('event background color');
                    
                    colorTag.html(tag);
            });


            $('#edit-color-chooser > li > a').click(function (e) {
                e.preventDefault();
                currColor = $(this).css('color')
                $('#editEventColorTag').val(currColor);
                tag.css({
                    'background-color': currColor,
                    'border-color'    : currColor,
                    'color'           : '#fff'
                }).addClass('external-event');
                tag.html('Selected Event Background-Color');
                colorTag.html(tag);
            });


            //call off event to prevent propagation of multiple notifyjs box
            $('#btnEditEventSubmit').off('click').on('click', function(e) {
            
                var params = {
                    urlMethod : 'POST',
                    apiURL    : '/admin/update-event/'+ paramId,
                    data      : AdminUtils.constructPayloadData('#form-edit-event'),
                };
    
                AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                    if (xhrResult[0].status === 200) {
                        $('#modal-edit').modal('hide');
                        self.loadEventTableData();
                        $.notify("Event Updated Sucessfully!", "success");
                    }
                    else
                        alert('Error! Please contact your system administrator');
                });

                e.preventDefault();
            });
    },


    /**
     * 
     * Delete Event Modal
     * 
     */
    deleteEventModalHandler: function(context, event) {
        var self        = this,
            modalDelete = $('#modal-delete'),
            deleteURL   = '/admin/delete-event/',
            getURL      = '/admin/get-event/',
            paramId     = $(context).data('value');

            modalDelete.modal({show: true});
            
            self.getJSONCallback(getURL + paramId, false, function(data) {
                
                var evtDateStart = moment(new Date(data.start_date)),
                    evtDateEnd   = moment(new Date(data.end_date)),
                    clockDOM     = '<i class="fa fa-clock-o"></i> ',
                    approvedDOM  = '<span class="label label-success"><i class="fa fa-check"></i> APPROVED</span>',
                    pendingDOM   = '<span class="label label-warning"><i class="fa fa-clock-o"></i> PENDING</span>',
                    status  = (data.status === 'APPROVED') ? approvedDOM : pendingDOM;


                $('#deleteTitle').html(data.title.toUpperCase());
                $('#deleteDateStart').html(evtDateStart.format('MMMM DD[,] YYYY'));
                $('#deleteTimeStart').html(clockDOM + evtDateStart.format('h:mm a'));
                $('#deleteTimeEnd').html(clockDOM + evtDateEnd.format('h:mm a'));
                $('#deleteDateEnd').html(evtDateEnd.format('MMMM DD[,] YYYY'));
                $('#deleteStatus').html(status);
                $('#deleteDescription').html(data.description);
            });

            $('#btnDeleteEvent').off('click').on('click', function(e) {
                var params = {
                    urlMethod : 'POST',
                    apiURL    : deleteURL + paramId,
                    data      : {eventStatus: 'REMOVED'},
                };
    
                AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                    if (xhrResult[0].status === 200) {
                        $('#modal-delete').modal('hide');
                        $.notify("Event Removed Sucessfully!", "info");
                        self.loadEventTableData();
                    }
                    else
                        alert('Error! Please contact your system administrator');
                });
                e.preventDefault();
            });
    },

    /**
     * 
     * Add new event button handler
     * 
     * 
     */
    addEventModalHandler: function() {
        var self = this;

        $('#btn-add-event').click(function() {
            $('#modal-form').modal({show: true});
        });

        $('#color-chooser > li > a').click(function (e) {
            e.preventDefault();
            currColor = $(this).css('color')
            $('#eventColorTag').val(currColor);

            var tag   = $('<div />'),
            colorTag   = $('#selColorTag')
            tag.css({
                'background-color': currColor,
                'border-color'    : currColor,
                'color'           : '#fff'
            }).addClass('external-event');
            tag.html('Selected Event Background-Color');
            colorTag.html(tag);
        });

        /**
         * handle add event on modal
         */
        $('#btnAddEventSubmit').on('click', function(e) {
           
            var params = {
                urlMethod : 'POST',
                apiURL    : '/admin/save-event',
                data      : AdminUtils.constructPayloadData('#form-add-event'),
            };

            AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                if (xhrResult[0].status === 200) {
                    $('#modal-form').modal('hide');
                    $.notify("New Event Added Sucessfully!", "success");
                    self.loadEventTableData();
                }
                else {
                    alert('Error! Please Contact Your System Administrator');
                    console.log(xhrResult[0].responseText);
                }
            });
            e.preventDefault();
        });

    },

    loadEventTableData: function() {
        var self      = this,
            statusFlag = null;
            
        self.getJSONCallback('/admin/get-all-events' , true, function(data) {
            if ($.fn.dataTable.isDataTable(oTable)) oTable.DataTable().destroy();
            oTable.dataTable({
                responsive: true,
                data: data,
                aaSorting: [[ 6, "desc" ]],
                aoColumns: [
                    {sName: data[0], mRender: 
                        function(d) {
                            return '<b>' + d + '</b>';  
                        },
                    },
                    {sName: data[1], mRender: 
                        function(d) {
                            var dt = new Date(d);
                            return dt.toLocaleString();
                        }
                    },
                    {sName: data[2], mRender: 
                        function(d) {
                            var dt = new Date(d);
                            return dt.toLocaleString();
                        }
                    },
                    {sName: data[3], mRender: 
                        function(d) {
                            var pending  = '<span class="label label-warning"><i class="fa fa-clock-o"></i> &nbsp;&nbsp;&nbsp;PENDING</span>';
                            var approved = '<span class="label label-success"><i class="fa fa-check"></i> APPROVED</span>';
                            var removed  = '<span class="label label-danger"><i class="fa fa-times"></i> &nbsp;&nbsp;&nbsp;REMOVED</span>';
                            
                            if (d === 'APPROVED') {
                                statusFlag = 'APPROVED';
                                return approved;
                            } else if (d === 'PENDING') {
                                statusFlag = 'PENDING';
                                return pending;
                            } else {
                                statusFlag = 'REMOVED';
                                return removed;
                            }
                        }
                    },
                    {sName: data[4], mRender: 
                        function(d) {
                            var dt = new Date(d);
                            return dt.toLocaleString();
                        }
                    },
                    {sName: data[5], mRender: 
                        function(d) {
                            return d;
                        } 
                    },                 
                    {sName: data[6], mRender: 
                        function(d) {
                            var btnEditDOM   = '<button class="btn btn-primary btn-xs btnEdit" data-title="Edit" data-target="#edit" data-value="'+ d +'" onClick="Bulletin.App.Admin.editEventModalhandler(this, event);"><span class="glyphicon glyphicon-pencil"></span></button>&nbsp;',
                                btnDeleteDOM = '<button class="btn btn-danger btn-xs btnDelete" data-title="Delete" data-target="#delete" data-value="'+ d +'" onClick="Bulletin.App.Admin.deleteEventModalHandler(this, event)"><span class="glyphicon glyphicon-trash"></span></button>';
                    
                            if (statusFlag === 'REMOVED') 
                                return btnEditDOM;
                            else
                                return btnEditDOM + btnDeleteDOM;
                        }
                    },
                ]
            });
        });
    },

    getJSONCallback: function(apiURL, dataTableFlag,  callback) {
        var self = this,
            url  = apiURL + '?token=' + gToken;
        $.getJSON(url, function (list) {
            if (dataTableFlag === true) {
                var objData = self.constructEventObject(list);
                callback(objData);
            } else {
                callback(list);
            }
        });
    },
    

    /**
     * ConstructEventObject for Datatable
     *  arrange data from API into acceptable datatable format
     * 
     */
    constructEventObject: function(data) {
        var arrList = [];

        for (var i in data) {
            var curArr = [];
            //if (data[i].status !== 'REMOVED') {
                curArr.push(data[i].title);
                curArr.push(data[i].start_date);
                curArr.push(data[i].end_date);
                curArr.push(data[i].status);
                curArr.push(data[i].createdAt);
                curArr.push(data[i].createdBy);
                curArr.push(data[i].eventId);
                arrList.push(curArr);
            //}
        }
        return arrList;
    },
};


(function() {
    Bulletin.App.Admin.fnDashboardLoader();
})();
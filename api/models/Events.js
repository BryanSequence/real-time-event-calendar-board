/**
 * Events.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
     connection: 'mysql',
      tableName: 'tbl_events',
         autoPK: false,
     attributes: {
        eventId      : { 
        type         : 'integer', 
        unique       : true, 
        primaryKey   : true, 
        autoIncrement: true, 
    },
    title       : {type:  'string'  },
    description : {type:  'string'  },
    start_date  : {type:  'datetime'},
    end_date    : {type:  'datetime'},
    colorTag    : {type:  'string'  },
    status      : {
        type:  'string',
        enum: ['APPROVED', 'PENDING', 'REMOVED']
    },
    createdAt   : {type:   'datetime'},
    createdBy   : {type:   'string'}
  }
};


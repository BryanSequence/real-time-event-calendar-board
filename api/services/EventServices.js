/**
 * 
 * EventServices.js
 * 
 * @author Bryan Judelle Ramos
 * @description User Services, do basic DB related query and passed to controller
 *              this contains all the DATA MODEL ABSTRACTION LAYER ONLY.
 * 
 * 
 */

module.exports = {
    saveEventInfo: function(params, callback) {
        Events.create(params).exec(function(err, event) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.saveEventInfo {ERROR on SAVING Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(event);
        });
    },

    updateEventInfo: function(paramsId, paramsValue, callback) {
        Events.update(paramsId, paramsValue).exec(function(err, event) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.updateEventInfo {ERROR on UPDATE Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(event);
        });
    },

    getAllEvents: function(callback) {
        Events.find().exec(function(err, listEvent) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.getAllEvents {ERROR on RETRIEVING Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(listEvent);
        });
    },

    findEvent: function(id, callback) {
        var params = { eventId : id};
        Events.findOne(params).exec(function(err, eventInfo) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.findEvent {ERROR on SEARCH Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(eventInfo);
        });
    },

    findEventFilterBy: function(params, callback) {
        var opts = {
            sql  : 'SELECT * FROM tbl_events WHERE ' + params.reference_field + ' = ?',
            value: params.field_value 
        };

        Events.query(opts.sql, opts.value, function(err, rawResult) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.findEventFilterBy {ERROR on GENERIC QUERY Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(rawResult);
        });
    },

    findEventByQuery: function(params, callback) {
        var opts = {
            sql  : params.sql_query,
        };

        Events.query(opts.sql, function(err, rawResult) {
            if (err) {
                var opts = {
                    error    : true,
                    origin   : 'EventServices.findEventByQuery {ERROR on GENERIC QUERY(2) Event INFO}',
                    message  : Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(rawResult);
        });
    },
};
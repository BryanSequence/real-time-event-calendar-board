/**
 * EventController
 *
 * @description :: Server-side logic for managing Events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var _ = require('underscore');

module.exports = {
    saveEvent: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        
        var evtDateTime = params.eventTime.split(" - "),
            evtStatus   = (params.eventStatus === 'on') ? 'APPROVED' : 'PENDING';
        
        var paramObj = {
            title       : params.eventTitle,
            description : params.eventDescription,
            start_date  : evtDateTime[0],
            end_date    : evtDateTime[1],
            colorTag    : params.eventColorTag,
            status      : evtStatus,
            createdBy   : params.user
        };
        
        var response = EventServices.saveEventInfo(paramObj, function (err, event) {    
                
            if (err.error)
                return res.serverError(err.message, err.origin);
            else 
                return res.json(event);
        });
    },

    updateEventInfo: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        
        var paramId     = {eventId: params.id},
            evtDateTime = params.eventTime.split(" - "),
            evtStatus   = (params.eventStatus === 'on') ? 'APPROVED' : 'PENDING';
    
        var paramObj = {
            title       : params.eventTitle,
            description : params.eventDescription,
            start_date  : evtDateTime[0],
            end_date    : evtDateTime[1],
            colorTag    : params.eventColorTag,
            status      : evtStatus
        };

        var response = EventServices.updateEventInfo(paramId, paramObj, function (err, event) {             
            if (err) return err;
            return event;
        });

        return res.json(response); 
    },

    deleteEventInfo: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        var paramId     = {eventId: params.id},
            paramObj    = {status : params.eventStatus};

        var response = EventServices.updateEventInfo(paramId, paramObj, function (err, event) {             
            if (err) return err;
            return event;
        });

        return res.json(response); 
    },

    getAllEvents: function(req, res) {
        var response = EventServices.getAllEvents(function(event){
            return res.json(event);
        });
    },

    getEventById: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        var response = EventServices.findEvent(params.id, function(event){
            return res.json(event);
        });
    },

    findEventByGenericQuery: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        if (!params.reference_field || !params.field_value)
            return res.json(400, {err: 'reference_field and field_value are required'});
 
        var opts = {
            reference_field: params.reference_field,
            field_value: params.field_value
        };
        var response = EventServices.findEventFilterBy(opts, function(event){
            return res.json(event);
        });

    }
};


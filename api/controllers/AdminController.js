/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admincontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore'),
path = require('path');

module.exports = {
    
    login: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        
        var user = params.username,
            pass = params.password,
            paramsObj = {
                username: user
            };

        var result = UserServices.findUserByQuery(paramsObj, function(results) {

            if (results !== undefined) {
                
                if (results.error !== undefined && results.error) return res.json(results);

                var validateLogin = Utils.decryptPassword(pass,results.password);

                if (validateLogin) {
                    //for long and safe token
                    //var payload = results.userId + new Date();
                    var token = Utils.issue({id: results.userId}),
                        userId= results.userId,
                        userRole = results.role;
                        userFullname = results.fullname;
                    return res.json({userId: userId, fullname: userFullname,  role: userRole, auth_token: token});
                } else {
                    return res.json(401,{
                    err: 'Invalid username or password'
                    });
                }
            } else {
                return res.json(401, {err: 'Invalid username or password'});
            }
    
        });
    },

    logout: function(req, res) {
       return  res.json({ response: 'ok'});
    },

    getAllEvents: function(req, res) {
        var params = {
            sql_query: "SELECT * FROM tbl_events WHERE end_date >= NOW() and status <> 'REMOVED'"
        };
        var response = EventServices.findEventByQuery(params,function(event){
            return res.json(event);
        });
    },

    saveAnnouncement: function(req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        console.log(params);

        if (req.method === 'GET') return res.json({ 'status': 'GET not allowed' });
        
        var protocol   = req.connection.encrypted ? 'https' : 'http',
            base_url   = protocol + '://' + req.headers.host + '/',
            uploadFile = req.file('img_announcement');
            
            uploadFile.upload({ 
                dirname: process.cwd() + sails.config.globals.ASSET_UPLOAD_PATH,
                saveAs: function (__newFileStream, cb) {
                    cb(null, new Date().getTime() + '_' + 'announcement' + path.extname(__newFileStream.filename));
                },
            }, function onUploadComplete(err, files) {
                
                if (err) return res.serverError(err);
                
                var filename       = files[0].fd.substring(files[0].fd.lastIndexOf('/') + 1),
                    uploadLocation = process.cwd() + sails.config.globals.ASSET_UPLOAD_PATH + filename,
                    tempLocation   = process.cwd() + sails.config.globals.TMP_UPLOAD_PATH   + filename,
                    image_url      = base_url + filename.split(sails.config.globals.PROJ_ASSET_PATH)[1].split('\\').join('/');

                if (!image_url) return res.serverError('image url is missing');
                
                var paramBatchObj = {
                    announcement_txt : params.announcement_text ? params.announcement_text : '',
                    announcement_img : image_url
                };


                return res.json(paramBatchObj);
            });
        
    }

};


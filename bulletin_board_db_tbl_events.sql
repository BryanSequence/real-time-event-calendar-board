-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bulletin_board_db
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_events`
--

DROP TABLE IF EXISTS `tbl_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_events` (
  `eventId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `colorTag` varchar(45) DEFAULT '#0073b7',
  `status` enum('APPROVED','PENDING','REMOVED') NOT NULL DEFAULT 'PENDING',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `createdBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_events`
--

LOCK TABLES `tbl_events` WRITE;
/*!40000 ALTER TABLE `tbl_events` DISABLE KEYS */;
INSERT INTO `tbl_events` VALUES (1,'event title 1','test description 1','2018-01-03 00:00:00','2018-01-04 00:00:00','rgb(240, 18, 190)','REMOVED','2018-01-03 18:22:25',NULL),(2,'event title 3','test 3','2018-01-04 00:00:00','2018-01-06 23:00:00','rgb(255, 133, 27)','REMOVED','2018-01-03 18:29:02',NULL),(3,'event title 3','test 3','2018-01-04 00:00:00','2018-01-06 23:00:00','rgb(0, 166, 90)','REMOVED','2018-01-03 18:33:17',NULL),(4,'event title final 123','test 3333333','2018-01-04 00:00:00','2018-01-06 23:00:00','rgb(0, 166, 90)','APPROVED','2018-01-03 18:39:34',NULL),(5,'enrollment s.y 2017','sssss','2018-01-03 00:00:00','2018-01-04 00:00:00','rgb(240, 18, 190)','PENDING','2018-01-03 18:43:10',NULL),(6,'enrollment s.y 2017','sssss','2018-01-03 00:00:00','2018-01-04 00:00:00','rgb(0, 166, 90)','PENDING','2018-01-03 18:44:59',NULL),(7,'sss','test','2018-01-03 00:00:00','2018-01-03 23:59:00','rgb(96, 92, 168)','APPROVED','2018-01-03 18:46:24',NULL),(8,'sss','test','2018-01-03 00:00:00','2018-01-03 23:59:00','rgb(96, 92, 168)','APPROVED','2018-01-03 18:46:31',NULL),(9,'Prelims','exam period for summer class','2018-03-14 00:00:00','2018-03-15 23:00:00','rgb(0, 192, 239)','APPROVED','2018-01-03 18:48:24',NULL),(10,'qwerert','qweqwrrt','2018-01-06 00:00:00','2018-01-06 23:59:00','rgb(0, 31, 63)','PENDING','2018-01-06 03:07:45',NULL),(11,'qwerty2','qwerty desc','2018-01-06 00:00:00','2018-01-06 23:59:00','rgb(255, 133, 27)','APPROVED','2018-01-06 03:16:42',NULL),(12,'qwertsafafa','dasdasdasdada','2018-01-06 00:00:00','2018-01-06 23:59:00','rgb(240, 18, 190)','APPROVED','2018-01-06 03:19:54',NULL),(13,'no more blank title','Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...','2018-02-21 00:00:00','2018-02-22 00:00:00','','APPROVED','2018-01-06 03:21:10',NULL),(14,'14','event','2018-03-01 00:00:00','2018-03-10 23:00:00','rgb(96, 92, 168)','APPROVED','2018-01-06 03:21:41',NULL),(15,'15','15','2018-02-15 00:00:00','2018-02-15 23:00:00','rgb(221, 75, 57)','PENDING','2018-01-06 03:25:20',NULL),(16,'this is new event','test this new event description with notify','2018-01-25 00:00:00','2018-01-26 23:00:00','rgb(0, 166, 90)','PENDING','2018-01-06 17:19:42',NULL),(17,'Tagis Lakas','The New Era University established the TAGIS LAKAS. This TAG','2018-01-31 10:00:00','2018-02-03 17:00:00','rgb(255, 133, 27)','PENDING','2018-01-07 01:28:25',NULL),(18,'this is new event','test this new event description with notify','2018-01-25 00:00:00','2018-01-26 23:00:00','rgb(0, 166, 90)','PENDING','2018-01-07 02:41:11',NULL),(19,'sample','','2018-01-23 00:00:00','2018-01-23 00:30:00','rgb(240, 18, 190)','APPROVED','2018-01-07 21:59:11',NULL),(20,'new sample event','','2018-02-04 00:00:00','2018-02-05 23:00:00','rgb(221, 75, 57)','PENDING','2018-01-08 01:58:34',NULL),(21,'sample event 12','sample','2018-01-13 06:00:00','2018-01-15 23:00:00','rgb(0, 166, 90)','PENDING','2018-01-10 21:17:29',NULL);
/*!40000 ALTER TABLE `tbl_events` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15  0:04:07
